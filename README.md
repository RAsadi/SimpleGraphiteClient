# SimpleGraphiteClient

Easy to use, scala based graphite client, to send metrics to graphite.

## Use

```
val client = new TcpGraphiteClient("host.name",2003,"prefix")
val client2 = new UdpGraphiteClient("host.name",2004)
client.increment("unit.test",1.0)
client.increment("unit.test",1.0)
client2.increment("unit.test",2.0,1535116284) //including a timestamp to send out with
client.write()
client2.write()
```
This creates 2 graphite clients, one Tcp and one Udp. The increment method aggregates any stats, and the write method actually sends them out to
the server.


## Tests

Run tests from SBT. Currently, tests insure that both clients send out a correct message to a mock server

## License

This project is licensed under the MIT License - see the LICENSE file for details
