package com.github.RAsadi.SimpleGraphiteClient.client

import java.net.InetSocketAddress

import scala.collection.mutable.HashMap

abstract class GraphiteClient {
  def host : String
  def port : Int
  def prefix : String

  val address : InetSocketAddress = new InetSocketAddress(host,port)
  val stats : HashMap[(String,Long),Double] = HashMap.empty[(String,Long),Double]
  val defaultTime : Long = System.currentTimeMillis()/1000

  def increment(tag : String, amount : Double = 1.0, time : Long = defaultTime): String ={
    val increment : Double = stats.getOrElse((tag,time),0)
    stats += (((tag,time),increment+amount))
    if (prefix == null) s"""$tag $amount $time\n""" else s"""$prefix.$tag $amount $time\n"""
  }

  def getMsg(tag : String, amount : Double, time : Long): String =
    if (prefix == null) s"""$tag $amount $time\n""" else s"""$prefix.$tag $amount $time\n"""

  def close() : Unit
}
