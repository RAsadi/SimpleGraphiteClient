package com.github.RAsadi.SimpleGraphiteClient.client

import java.net.{DatagramPacket, DatagramSocket}

class UdpGraphiteClient(val host : String = "localhost", val port : Int = 2003, val prefix : String = null) extends GraphiteClient {

  val remote : DatagramSocket = new DatagramSocket()

  def write(): Unit ={
    stats.foreach { stat =>
      val tag = stat._1._1
      val time = stat._1._2
      val amount = stat._2
      val msg = getMsg(tag, amount, time)
      val packet: DatagramPacket = new DatagramPacket(msg.getBytes, msg.length, address)
      remote.send(packet)
    }
  }

  def close = remote.close()
}