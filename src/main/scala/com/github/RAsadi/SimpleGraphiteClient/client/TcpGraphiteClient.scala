package com.github.RAsadi.SimpleGraphiteClient.client

import java.io.OutputStream
import java.net.Socket

class TcpGraphiteClient(val host : String = "localhost", val port : Int = 2003, val prefix : String = null) extends GraphiteClient{

  val remote : Socket = new Socket(address.getAddress,address.getPort)
  val outStream : OutputStream = remote.getOutputStream

  def write(): Unit ={
    stats.foreach{ stat =>
      val tag = stat._1._1
      val time = stat._1._2
      val amount = stat._2

      val msg = getMsg(tag, amount, time)

      outStream.write(msg.getBytes)
    }
    outStream.flush()
  }

  def close = remote.close()
}