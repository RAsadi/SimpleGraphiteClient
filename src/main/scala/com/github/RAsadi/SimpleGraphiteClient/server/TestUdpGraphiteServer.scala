package com.github.RAsadi.SimpleGraphiteClient.server

import java.net.{DatagramPacket, DatagramSocket}

class TestUdpGraphiteServer(port : Int){
  val server = new DatagramSocket(port)
  val SIZE = 1024

  def testInput : String = {
    val buffer = new Array[Byte](SIZE)
    val packet = new DatagramPacket(buffer, buffer.length)
    server.receive(packet)
    val message = new String(packet.getData)
    message
  }

  def close = server.close()
}
