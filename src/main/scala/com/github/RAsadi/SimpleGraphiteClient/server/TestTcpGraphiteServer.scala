package com.github.RAsadi.SimpleGraphiteClient.server

import java.net.ServerSocket

import scala.io.BufferedSource

class TestTcpGraphiteServer(port : Int){

  val server = new ServerSocket(port)

  def testInput: String =  {
    val socket = server.accept
    val input = new BufferedSource(socket.getInputStream).getLines().next
    socket.shutdownOutput()
    socket.close()
    input
  }

  def close = server.close()

}