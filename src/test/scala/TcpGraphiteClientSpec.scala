import com.github.RAsadi.SimpleGraphiteClient.client.TcpGraphiteClient
import com.github.RAsadi.SimpleGraphiteClient.server.TestTcpGraphiteServer
import org.scalatest.FunSuite

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

class TcpGraphiteClientSpec extends FunSuite{

  test("insure that the client sends out a proper packet"){
    val server = new TestTcpGraphiteServer(9998)
    val result = Future {server.testInput}
    val client = new TcpGraphiteClient("localhost",9998)
    val msg = client.increment("unit.test")
    client.write()

    result.onComplete{
      case Success(serverIn) =>
        client.close
        server.close
        assert(serverIn.trim === msg.trim)
      case Failure(exception) =>
        client.close
        server.close
        fail(exception)
    }

    Await.ready(result,Duration.apply("10s"))
  }

}
