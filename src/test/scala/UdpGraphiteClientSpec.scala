import com.github.RAsadi.SimpleGraphiteClient.client.UdpGraphiteClient
import com.github.RAsadi.SimpleGraphiteClient.server.TestUdpGraphiteServer
import org.scalatest.FunSuite

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

class UdpGraphiteClientSpec extends FunSuite{

  test("insure that the client sends out a proper packet"){
    val server = new TestUdpGraphiteServer(9999)
    val result = Future {server.testInput}
    val client = new UdpGraphiteClient("localhost",9999)
    val msg = client.increment("unit.test")
    client.write()

    result.onComplete{
      case Success(serverIn) =>
        client.close
        server.close
        assert(serverIn.trim === msg.trim)
      case Failure(exception) =>
        client.close
        server.close
        fail(exception)
    }

    Await.ready(result,Duration.apply("10s"))
  }
}

